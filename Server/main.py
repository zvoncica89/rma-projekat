import flask, json
from flask import Flask, request
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor
import datetime

mysql_db = MySQL(cursorclass=DictCursor)

app = Flask('__main__', template_folder="", static_folder="", root_path="", static_url_path="")


app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'mydb'

mysql_db.init_app(app)
usernames = []

@app.route("/")
def index():
    return app.send_static_file("index.html")
    
def provera_pristupa(uloga):
    dozvoli = False
    if len(usernames) > 0:
        for username in usernames:
            if flask.session.get(username) is not None and flask.session.get(username) == uloga:
                dozvoli = True
    if dozvoli:
        return True
    else:
        return False

@app.route("/login", methods=["POST"])
def login():
    data = dict()
    app_response = request.get_json()
    data["korisnicko_ime"] = app_response["korisnicko_ime"]
    data["lozinka"] = app_response["lozinka"]
    # print(data)

    if not data["korisnicko_ime"] in usernames:
        cr = mysql_db.get_db().cursor()
        cr.execute("SELECT * FROM korisnici WHERE korisnicko_ime=%(korisnicko_ime)s AND lozinka=%(lozinka)s", data)
        user = cr.fetchone()
        cr.execute("SELECT * FROM prava_pristupa_has_korisnici")
        # print(user)
        prava_pristupa = cr.fetchall()
        if user is not None:
            for i in range(0, len(prava_pristupa)):
                if prava_pristupa[i]["korisnici_id"] == user["id"]:
                    cr.execute("SELECT opis FROM prava_pristupa WHERE id="+str(prava_pristupa[i]["prava_pristupa_id"]))
                    user["prava"] = cr.fetchone()["opis"]
            # flask.session[user["korisnicko_ime"]] = user["prava"]
            usernames.append(user["korisnicko_ime"])
            return user["prava"], 200
            # return "", 200
    
    return "neuspesno", 200

@app.route("/dobaviSvaPica", methods=["GET"])
def dobavi_pica():
    cr = mysql_db.get_db().cursor()
    upit = ("SELECT * FROM pice")
    cr.execute(upit)
    pica = cr.fetchall()
    # print(pica)

    return flask.json.jsonify(pica)

@app.route("/dobaviPicaPoKategoriji", methods=["POST"])
def dobavi_pica_narucivanje():
    tip_pica = request.get_json()
    id_tipa_pica = tip_pica["tip_pica"]
    cr = mysql_db.get_db().cursor()
    # upit = ("SELECT * FROM pice WHERE tip_pica_id_tipa_pica=%s and kolicina > 0", (id_tipa_pica,))
    # print(upit)
    cr.execute("SELECT * FROM pice WHERE tip_pica_id_tipa_pica=%s and kolicina > 0", (id_tipa_pica,))
    pica = cr.fetchall()
    # print(pica)
    return flask.json.jsonify(pica)

@app.route("/dobaviSveKorisnike", methods=["GET"])
def dobavi_korisnike():
    cr = mysql_db.get_db().cursor()
    upit = ("select * from korisnici inner join prava_pristupa inner join prava_pristupa_has_korisnici where korisnici.id = prava_pristupa_has_korisnici.korisnici_id and prava_pristupa_id = prava_pristupa.id")
    cr.execute(upit)
    korisnici = cr.fetchall()
    # print(korisnici)

    return flask.json.jsonify(korisnici)

@app.route("/logout", methods=["POST"])
def logout():
    korisnik = request.get_json()
    userName = korisnik["korisnicko_ime"]
    try:
        usernames.remove(userName)
    except:
        return "logout error", 404
    # print(usernames)
    return "uspesno izlogovani", 200

@app.route("/obrisiKorisnika", methods=["POST"])
def obrisi_korisnika():
    podatak = request.get_json()
    id_korisnika = podatak["id"]
    korisnicko_ime = podatak["ime"]
    # print(usernames)
    
    if korisnicko_ime not in usernames:     
        db = mysql_db.get_db()
        cr = mysql_db.get_db().cursor()   
        cr.execute("delete from prava_pristupa_has_korisnici where korisnici_id=%s", (id_korisnika,))
        cr.execute("DELETE FROM korisnici WHERE id=%s", (id_korisnika,))
        db.commit()
        return "uspesno obrisan", 200
    else:
        return "neuspesno brisanje", 200

@app.route("/obrisiPice", methods=["POST"])
def obrisi_pice():
    podatak = request.get_json()
    id_pica = podatak["id_pica"]
    db = mysql_db.get_db()
    cr = mysql_db.get_db().cursor()
    cr.execute("DELETE FROM stavka_narudzbine WHERE pice_id_pica=%s",(id_pica,) )
    cr.execute("DELETE FROM pice WHERE id_pica=%s", (id_pica,))
    db.commit()
    return "uspesno obrisano pice", 200

@app.route("/dodavanjeKorisnika", methods=["POST"])
def dodavanje_korisnika():
    novi_korisnik = request.get_json()
    # print(novi_korisnik)
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO korisnici (korisnicko_ime, ime, prezime, lozinka) VALUES(%s, %s, %s,  %s)", (novi_korisnik["korisnicko_ime"], novi_korisnik["ime"], novi_korisnik["prezime"], novi_korisnik["lozinka"]))
    cr.execute("SELECT id FROM korisnici WHERE korisnicko_ime=\""+novi_korisnik["korisnicko_ime"]+"\"")
    novi_korisnik["id_korisnika"] = cr.fetchone()["id"]
    cr.execute("INSERT INTO prava_pristupa_has_korisnici (prava_pristupa_id, korisnici_id) VALUES(%s, %s)", (novi_korisnik["uloga"], novi_korisnik["id_korisnika"]))
    db.commit()
    return "dodat novi korisnik", 201

@app.route("/dodavanjePica", methods=["POST"])
def dodavanje_pica():
    novo_pice = request.get_json()
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO pice (ime_pica, opis_pica, kolicina, cena, tip_pica_id_tipa_pica) VALUES(%s, %s, %s, %s, %s)", (novo_pice["ime_pica"], novo_pice["opis_pica"], novo_pice["kolicina"], novo_pice["cena"], novo_pice["tip_pica"]))
    db.commit()
    return "uspesno dodato pice", 201

@app.route("/izmenaKorisnika", methods=["POST"])
def izmena_korisnika():
    korisnik = request.get_json()
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict()
    data["ime"] = korisnik["ime"]
    data["prezime"] = korisnik["prezime"]
    data["korisnicko_ime"] = korisnik["korisnicko_ime"]
    cr.execute("UPDATE korisnici SET ime=%(ime)s, prezime=%(prezime)s WHERE korisnicko_ime=%(korisnicko_ime)s", data)
    db.commit()
    return "uspesno izmenjen korisnik", 200

@app.route("/izmenaPica", methods=["POST"])
def izmena_pica():
    pice = request.get_json()
    # print(pice)
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict()
    data["ime_pica"] = pice["ime_pica"]
    data["opis_pica"] = pice["opis_pica"]
    data["kolicina"] = pice["kolicina"]
    data["cena"] = pice["cena"]
    data["tip_pica_id_tipa_pica"] = pice["tip_pica"]
    cr.execute("UPDATE pice SET opis_pica=%(opis_pica)s, kolicina=%(kolicina)s, cena=%(cena)s, tip_pica_id_tipa_pica=%(tip_pica_id_tipa_pica)s  WHERE ime_pica=%(ime_pica)s", data)
    db.commit()
    return "uspesno izmenjeno pice", 200

@app.route("/kategorije", methods=["GET"])
def dobavi_kategorije():
    cr = mysql_db.get_db().cursor()
    upit = ("select * from tip_pica")
    cr.execute(upit)
    kategorije = cr.fetchall()
    return flask.json.jsonify(kategorije)

@app.route("/novaPorudzbina", methods=["POST"])
def dodavanje_porudzbine():
    nova_porudzbina = request.get_json()
    db = mysql_db.get_db()
    cr = db.cursor()
    narudzbina = dict()
    narudzbina["datum"] = datetime.datetime.now().replace(microsecond=0)
    narudzbina["status"] = 1
    narudzbina["id_uredjaja"] = 1
    narudzbina["ukupna_cena"] = nova_porudzbina["ukupna_cena"]
    # narudzbina["ukupna_cena"] = request.json["ukupnaCena"]
    cr.execute("INSERT INTO narudzbina (datum, status, uredjaj_id_uredjaja, ukupna_cena) VALUES(%(datum)s, %(status)s, %(id_uredjaja)s, %(ukupna_cena)s)", narudzbina)
    datum = str(narudzbina["datum"]).split(".")
    narudzbina["datum"] = datum[0]
    cr.execute("SELECT id_narudzbine FROM narudzbina WHERE datum LIKE \"%" + narudzbina["datum"] + "%\"")
    id_porudzbine = cr.fetchone()
    db.commit()
    id = [id_porudzbine]
    return flask.json.jsonify(id), 201

@app.route("/dodajStavke", methods=["POST"])
def dodavanje_stavki():
    nova_stavka = request.get_json()
    db = mysql_db.get_db()
    cr = db.cursor()
    stavka = dict()
    stavka["kolicina"] = nova_stavka["kolicina"]
    stavka["pice_id_pica"] = nova_stavka["pice_id_pica"]
    stavka["cena"] = nova_stavka["cena"]
    stavka["narudzbina_id_narudzbine"] = int(nova_stavka["id_porudzbine"])
    cr.execute("INSERT INTO stavka_narudzbine (kolicina, pice_id_pica, cena, narudzbina_id_narudzbine) VALUES(%(kolicina)s, %(pice_id_pica)s, %(cena)s, %(narudzbina_id_narudzbine)s)", stavka)
    db.commit()
    return "", 200
    


# @app.route("/proba", methods=["GET"])
# def proba():
#     return "jej"

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)





