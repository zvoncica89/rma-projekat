package com.example.restoranapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.restoranapp.Controler.ApiHandler;
import com.example.restoranapp.Controler.HttpHandler;
import com.example.restoranapp.Controler.LoginControler;
import com.example.restoranapp.Controler.NarucivanjeControler;
import com.example.restoranapp.View.Narucivanje;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static Context mainActiityContext;
    public static boolean ulogovan;
    public static String ulogovaniKorisnik;
    private ArrayList<LinearLayout> children;
    LinearLayout mainLayout;
    LinearLayout subLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MainActivity.mainActiityContext = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!ulogovan){
            narucivanjePrikaz();
        }
        else {
            final LoginControler loginControler = LoginControler.getInstance();
            Button login = findViewById(R.id.login);
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loginControler.login();
                }
            });
        }





    }

    private void narucivanjePrikaz(){
        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(mainActiityContext);
        mainLayout = findViewById(R.id.izmenjiviLayout);
        mainLayout.removeAllViews();



        subLayout = (LinearLayout) inflater.inflate(R.layout.pocetna_strana, mainLayout, false);
        children.add(subLayout);

        Button login = subLayout.findViewById(R.id.pocetnoDugme);
        login.setText("Login");
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ulogovan = true;
                setContentView(R.layout.activity_main);
                Intent intent = new Intent(mainActiityContext, MainActivity.class);
                startActivity(intent);

            }
        });

        Button naruci = subLayout.findViewById(R.id.naruciDugme);
        final NarucivanjeControler narucivanjeControler = NarucivanjeControler.getInstance();
        naruci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.activity_narucivanje);
                Intent intent = new Intent(mainActiityContext, Narucivanje.class);
                startActivity(intent);

            }
        });

        mainLayout.addView(subLayout);

    }
}
