package com.example.restoranapp.Controler;

import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.restoranapp.MainActivity;
import com.example.restoranapp.R;
import com.example.restoranapp.View.PrikazPica;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginControler implements HttpHandler{
    private static LoginControler instance = null;
    private String korisnickoIme;

    public static LoginControler getInstance(){
        if (LoginControler.instance == null){
            LoginControler.instance = new LoginControler();
        }
        return LoginControler.instance;
    };

    private LoginControler() {
    }

    public void login(){
        AppCompatActivity mainActivityFields = ((AppCompatActivity) MainActivity.mainActiityContext);
        korisnickoIme = ((EditText) mainActivityFields.findViewById(R.id.korisnickoImeUnos)).getText().toString();
        String lozinka = ((EditText) mainActivityFields.findViewById(R.id.lozinkaUnos)).getText().toString();
        System.out.println(korisnickoIme);

        JSONObject json = new JSONObject();
        try {
            json.put("korisnicko_ime", korisnickoIme);
            json.put("lozinka", lozinka);
            ApiHandler.postRequest("login", json, (HttpHandler) LoginControler.instance);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onGetRequest(String result) {
        String r = result;
    }

    @Override
    public void onPostRequest(String result) {


        String r = result;
        if (r.equals("administrator")){
            MainActivity.ulogovan = true;
            MainActivity.ulogovaniKorisnik = korisnickoIme;
            Intent intent = new Intent(MainActivity.mainActiityContext, PrikazPica.class);
            ((AppCompatActivity) MainActivity.mainActiityContext).startActivity(intent);
            ((AppCompatActivity) MainActivity.mainActiityContext).finish();
        }
        if (r.equals("neuspesno")){
            Toast.makeText(MainActivity.mainActiityContext, "Pogresno uneti podaci!", Toast.LENGTH_LONG).show();
        }

    }

//    public void ugasi(){
//        ApiHandler.postRequest("ugasi", null, (HttpHandler) LoginControler.instance);
//    }
}
