package com.example.restoranapp.Controler;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class ApiHandler {
    public static String adresa = "http://192.168.2.105:5000/";

    public static void getRequest(String path, final HttpHandler httpHandler){
        AsyncTask<Object, Void, String> task = new AsyncTask<Object, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Object... params) {
                String response = "";
                try {
                    URL url = new URL(adresa + params[0]);

                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
//                    con.setRequestProperty("Content-Type", "application/json");
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    String red;
                    int i = 0;
                    while((red=br.readLine()) != null){
                        i+=1;
                        response += red;
                    }
                    br.close();
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                httpHandler.onGetRequest(s);
            }
        };
        task.execute(path);
    }

    public static void postRequest(String path, JSONObject json, final HttpHandler httpHandler){
        AsyncTask<Object, Void, String> task = new AsyncTask<Object, Void, String>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Object... params) {
                String response = "";
                OutputStream out = null;

                try {
                    URL link = new URL(adresa + params[0]);
                    HttpURLConnection connection = (HttpURLConnection) link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");

                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                    bw.write(((JSONObject)params[1]).toString());
                    bw.flush();
                    bw.close();

                    connection.getOutputStream().close();
                    connection.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row;
                    }

                    br.close();
                    connection.disconnect();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                httpHandler.onPostRequest(s);
            }
        };
        task.execute(path, json);
    }


}
