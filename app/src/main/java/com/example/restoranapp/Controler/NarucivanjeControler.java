package com.example.restoranapp.Controler;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.restoranapp.MainActivity;
import com.example.restoranapp.R;
import com.example.restoranapp.View.Narucivanje;
import com.example.restoranapp.View.PrikazPica;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.Iterator;

public class NarucivanjeControler implements HttpHandler {

    private static NarucivanjeControler instance = null;
    ArrayList<LinearLayout> children;
    LinearLayout mainLayout;
    LinearLayout subLayout;
    TextView imePicaPrikaz, opisPicaPrikaz, cenaPicaPrikaz;
    TextView stavkaIme, stavkaKolicina, stavkaUkupnaCena, ukupnaCenaPorudzbine;
    JSONArray svaPica;
    JSONArray porudzbina = new JSONArray();
    JSONObject punaCena = new JSONObject();
    private Double ukupnoPara = 0.0;


    public NarucivanjeControler() {
    }

    public Integer duzinaPorudzbine() {
        return porudzbina.length();
    }

    public static NarucivanjeControler getInstance() {
        if (NarucivanjeControler.instance == null) {
            NarucivanjeControler.instance = new NarucivanjeControler();
        }
        return NarucivanjeControler.instance;
    }

    public void naruciPica() {
        ApiHandler.getRequest("kategorije", (HttpHandler) NarucivanjeControler.instance);
    }
    public void dobaviPica() {
        ApiHandler.getRequest("dobaviSvaPica", (HttpHandler) NarucivanjeControler.instance);
    }



    @Override
    public void onGetRequest(String result) {
        String r = result;
        try {
            JSONArray jsonArr = new JSONArray(r);
            JSONObject prvi = null;
            prvi = (JSONObject) jsonArr.get(0);
            Iterator<String> kljucevi = (prvi.keys());
            while (kljucevi.hasNext()) {
                String kljuc = kljucevi.next();
                if (kljuc.equals("ime_tipa")) {
                    prikazKategorija(jsonArr);
                }
                if (kljuc.equals("id_pica")){
                    svaPica = jsonArr;
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPostRequest(String result) {
        String r = result;
        System.out.println(r);
        try {
//            try{
            JSONArray jsonArr = new JSONArray(r);
            JSONObject prvi = null;
            prvi = (JSONObject) jsonArr.get(0);
            Iterator<String> kljucevi = (prvi.keys());
            while (kljucevi.hasNext()) {
                String kljuc = kljucevi.next();
                if (kljuc.equals("id_pica")) {
                    prikazPicaPoKategoriji(jsonArr);
                }
                if(kljuc.equals("id_narudzbine")){
                    slanjeStavki(r);
                }
            }
//            } catch (JSONException e) {
//                JSONObject rezultat = new JSONObject(r);
//                Iterator<String>
//            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @SuppressLint("ResourceAsColor")
    public void prikazKategorija(JSONArray jsonArray) {
        AppCompatActivity narucivanjeActivity = ((AppCompatActivity) Narucivanje.narucivanjeActivityContent);

        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(narucivanjeActivity);
        mainLayout = narucivanjeActivity.findViewById(R.id.mainLayout);

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject json = null;
            try {
                json = (JSONObject) jsonArray.get(i);
                subLayout = (LinearLayout) inflater.inflate(R.layout.kategorije_inflater, mainLayout, false);
                children.add(subLayout);
                final Button kategorija = subLayout.findViewById(R.id.kategorijaPica);
                kategorija.setBackgroundColor(R.color.ljubicasto);
                kategorija.setText(json.get("ime_tipa").toString());

                final JSONObject finalJson = json;
                kategorija.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        JSONObject jsonKategorija = new JSONObject();
                        try {
                            jsonKategorija.put("tip_pica", finalJson.get("id_tipa_pica"));
                            ApiHandler.postRequest("dobaviPicaPoKategoriji", jsonKategorija, (HttpHandler) NarucivanjeControler.instance);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


            } catch (JSONException e) {
                e.printStackTrace();
            }
            mainLayout.addView(subLayout);


        }
    }

    public void prikazPicaPoKategoriji(JSONArray jsonArray) {
        AppCompatActivity narucivanjeActivity = ((AppCompatActivity) Narucivanje.narucivanjeActivityContent);
        TextView tekst = narucivanjeActivity.findViewById(R.id.tekstNarucivanja);
        tekst.setText("Pica");

        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(narucivanjeActivity);
        mainLayout = narucivanjeActivity.findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject json = null;
            try {
                json = (JSONObject) jsonArray.get(i);
                subLayout = (LinearLayout) inflater.inflate(R.layout.prikaz_po_kategoriji_inflater, mainLayout, false);
                children.add(subLayout);

                imePicaPrikaz = subLayout.findViewById(R.id.imePicaPrikaz);

                opisPicaPrikaz = subLayout.findViewById(R.id.opisPicaPrikaz);
                if (!json.get("opis_pica").equals(null)) {
                    opisPicaPrikaz.setText(json.get("opis_pica").toString());
                }

                cenaPicaPrikaz = subLayout.findViewById(R.id.cenaPicaPrikaz);
                imePicaPrikaz.setText(json.get("ime_pica").toString());
                cenaPicaPrikaz.setText(json.get("cena").toString()+"0 dinara");

                Button naruci = subLayout.findViewById(R.id.dodajNaPOrudzbinu);
                final JSONObject finalJson = json;
                naruci.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Integer id_pica = Integer.parseInt(finalJson.get("id_pica").toString());
                            Double cena = Double.parseDouble(finalJson.get("cena").toString());
                            dodajNaPorudzbinu(id_pica, cena, finalJson);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                if (i%2==0){
                    subLayout.setBackgroundColor(Color.parseColor("#D6E4F0"));
                } else {
                    subLayout.setBackgroundColor(Color.parseColor("#F6F6F6"));
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            mainLayout.addView(subLayout);
        }

    }
    private void dodajNaPorudzbinu(Integer idPica, Double cena, JSONObject json){
        AppCompatActivity narucivanjeActivity = ((AppCompatActivity) Narucivanje.narucivanjeActivityContent);
        JSONObject stavka = new JSONObject();
        Integer kolicina = 1;
        try {
            if (porudzbina.length() == 0){
                stavka.put("kolicina", kolicina);
                stavka.put("pice_id_pica", idPica);
                stavka.put("cena", cena);
                porudzbina.put(stavka);
                Toast.makeText(narucivanjeActivity, "Dodato na poruzbinu", Toast.LENGTH_SHORT).show();
                return;

            }
            if (porudzbina.length()>0){
                for(int i = 0; i < porudzbina.length(); i++){
                    JSONObject stavkaPorudzbine = (JSONObject) porudzbina.get(i);
                    if(stavkaPorudzbine.get("pice_id_pica") == idPica){
                        Integer prethodnaKolicina = Integer.parseInt(stavkaPorudzbine.get("kolicina").toString());
                        kolicina = prethodnaKolicina+1;
                        Integer ukupnaKolicina = Integer.parseInt(json.get("kolicina").toString());
                        if((ukupnaKolicina-kolicina)<0){
                            Toast.makeText(narucivanjeActivity, "Nema dovoljno pica na stanju!", Toast.LENGTH_LONG).show();
                            return;
                        }
                        stavkaPorudzbine.put("kolicina", kolicina);
                        Toast.makeText(narucivanjeActivity, "Dodato na poruzbinu", Toast.LENGTH_SHORT).show();

                        System.out.println(porudzbina);
                        return;
                    }


                }
                stavka.put("kolicina", kolicina);
                stavka.put("pice_id_pica", idPica);
                stavka.put("cena", cena);
                porudzbina.put(stavka);
                Toast.makeText(narucivanjeActivity, "Dodato na poruzbinu", Toast.LENGTH_SHORT).show();

            }
            System.out.println(porudzbina);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void prikazPorudzbine(){

        AppCompatActivity narucivanjeActivity = ((AppCompatActivity) Narucivanje.narucivanjeActivityContent);
        TextView tekst = narucivanjeActivity.findViewById(R.id.tekstNarucivanja);
        if (porudzbina.length() == 0){
            Toast.makeText(Narucivanje.narucivanjeActivityContent, "Porudzbina je prazna", Toast.LENGTH_SHORT).show();
            return;
        }

        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(narucivanjeActivity);
        mainLayout = narucivanjeActivity.findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();

        for (int i = 0; i < porudzbina.length(); i++) {
            JSONObject stavkaPorudzbine = null;
            try {
                stavkaPorudzbine = (JSONObject) porudzbina.get(i);
//                boolean provera = false;
                for (int j = 0; i < svaPica.length(); j++) {
                    JSONObject json;
                    json = (JSONObject) svaPica.get(j);
                    subLayout = (LinearLayout) inflater.inflate(R.layout.prikaz_porudzbine_inflater, mainLayout, false);
                    children.add(subLayout);
                    Integer idPicaJson = Integer.parseInt(json.get("id_pica").toString());
                    Integer idPicaPorudzbina = Integer.parseInt(stavkaPorudzbine.get("pice_id_pica").toString());
                    if(idPicaJson == idPicaPorudzbina){
                        stavkaIme = subLayout.findViewById(R.id.stavkaIme);
                        stavkaIme.setText(json.get("ime_pica").toString());

                        stavkaKolicina = subLayout.findViewById(R.id.stavkaKolicina);
                        stavkaKolicina.setText("Komada: "+stavkaPorudzbine.get("kolicina").toString());

                        stavkaUkupnaCena = subLayout.findViewById(R.id.stavkaUkupnaCena);
                        Double ukupnaCena = Integer.parseInt(stavkaPorudzbine.get("kolicina").toString()) * Double.parseDouble(stavkaPorudzbine.get("cena").toString());
                        ukupnoPara += ukupnaCena;
                        stavkaUkupnaCena.setText("Cena: "+ukupnaCena.toString()+"0 dinara");

                        mainLayout.addView(subLayout);
                        if (i%2==0){
                            subLayout.setBackgroundColor(Color.parseColor("#D6E4F0"));
                        } else {
                            subLayout.setBackgroundColor(Color.parseColor("#F6F6F6"));
                        }

                    }


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            tekst.setText("Ukupna cena: " + ukupnoPara.toString() +"0");

//            System.out.println(ukupnoPara.toString());

        }




    }

    public void zavrsiPorudzbinu(){
        try {
            for (int i = 0; i < porudzbina.length(); i++) {
                JSONObject stavka = (JSONObject) porudzbina.get(i);
                Double ukupnaCena = Integer.parseInt(stavka.get("kolicina").toString()) * Double.parseDouble(stavka.get("cena").toString());
                ukupnoPara += ukupnaCena;
                System.out.println(ukupnoPara.toString());

                System.out.println(punaCena);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            punaCena.put("ukupna_cena", ukupnoPara.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiHandler.postRequest("novaPorudzbina", punaCena, (HttpHandler) NarucivanjeControler.instance);


    }

    public void slanjeStavki(String id_porudzbine){
        AppCompatActivity narucivanjeActivity = ((AppCompatActivity) Narucivanje.narucivanjeActivityContent);
        if (porudzbina.length() == 0){
            Toast.makeText(narucivanjeActivity,"Porudzbina je prazna!", Toast.LENGTH_SHORT).show();
            return;
        }
        for (int i = 0; i<porudzbina.length(); i++){
            try {
                String id = id_porudzbine.split(":")[1].replace('}',':').split(":")[0];
                JSONObject stavka = (JSONObject) porudzbina.get(i);
                stavka.put("id_porudzbine", id);
                ApiHandler.postRequest("dodajStavke", stavka, (HttpHandler) NarucivanjeControler.instance);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        porudzbina = new JSONArray();
        Intent intent = new Intent(Narucivanje.narucivanjeActivityContent, MainActivity.class);
        ((AppCompatActivity) Narucivanje.narucivanjeActivityContent).startActivity(intent);
        ((AppCompatActivity) Narucivanje.narucivanjeActivityContent).finish();
    }
}

