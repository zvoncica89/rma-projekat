package com.example.restoranapp.Controler;

public interface HttpHandler {
    public void onGetRequest(String result);
    public void onPostRequest(String result);
}
