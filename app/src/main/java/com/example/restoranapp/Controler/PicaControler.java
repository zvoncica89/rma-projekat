package com.example.restoranapp.Controler;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.restoranapp.MainActivity;
import com.example.restoranapp.R;
import com.example.restoranapp.View.PrikazPica;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PicaControler implements HttpHandler {
    LinearLayout mainLayout;
    LinearLayout subLayout;
    private TextView imePica, detaljiPica, cenaPica, kolicinaPica, opcija;
    private TextView imeKorisnika, prezimeKorisnika, korisnickoIme, lozinka, ulogaKorisnika;
    private EditText opisPicaEdit, kolicinaPicaEdit, cenaPicaEdit;
    ArrayList<LinearLayout> children;
    private static PicaControler instance = null;




    public PicaControler() {
    }

    ;

    public static PicaControler getInstance() {
        if (PicaControler.instance == null) {
            PicaControler.instance = new PicaControler();
        }
        return PicaControler.instance;
    }

    ;

    public void dobaviPica() {
        ApiHandler.getRequest("dobaviSvaPica", (HttpHandler) PicaControler.instance);
    }
    public void dobaviKorisnike() {
        ApiHandler.getRequest("dobaviSveKorisnike", (HttpHandler) PicaControler.instance);
    }

    ;

    @Override
    public void onGetRequest(String result) {
        String r = result;
        try {
            JSONArray jsonArr = new JSONArray(r);
            JSONObject prvi = null;
            prvi = (JSONObject) jsonArr.get(0);
            Iterator<String> kljucevi = (prvi.keys());
            while(kljucevi.hasNext()){
                String kljuc = kljucevi.next();
                if (kljuc.equals("id_pica")){
                    generateData(jsonArr);
                }
                if (kljuc.equals("korisnicko_ime")){
                    generateDataKorisnici(jsonArr);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(r);
    }

    @Override
    public void onPostRequest(String result) {
        String r = result;
        if (r.equals("uspesno izlogovani")){
            MainActivity.ulogovan = false;
            Intent intent = new Intent(PrikazPica.picaActivityContent, MainActivity.class);
            ((AppCompatActivity) PrikazPica.picaActivityContent).startActivity(intent);
            ((AppCompatActivity) PrikazPica.picaActivityContent).finish();

        }
        if (r.equals("neuspesno brisanje")){
            Toast.makeText(PrikazPica.picaActivityContent, "Ne mozete obrisati sebe!", Toast.LENGTH_LONG).show();
        }
        if (r.equals("uspesno obrisan")){
            dobaviKorisnike();
        }
        if (r.equals("uspesno obrisano pice")){
            dobaviPica();
        }
        if (r.equals("dodat novi korisnik")){
            dobaviKorisnike();
        }
        if (r.equals("uspesno dodato pice")){
            dobaviPica();
        }
        if (r.equals("uspesno izmenjen korisnik")){
            dobaviKorisnike();
        }
        if(r.equals("uspesno izmenjeno pice")){
            dobaviPica();
        }


    }

    private void generateData(JSONArray jsonArr) {

        AppCompatActivity picaActivity = ((AppCompatActivity) PrikazPica.picaActivityContent);

        LinearLayout dodajL = picaActivity.findViewById(R.id.dugmeDodajLayout);
        LinearLayout prikaz = picaActivity.findViewById(R.id.prikazi);
        dodajL.setVisibility(View.VISIBLE);
        prikaz.setVisibility(View.VISIBLE);

        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(picaActivity);
        mainLayout = picaActivity.findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();

        for (int i = 0; i < jsonArr.length(); i++) {
            JSONObject json = null;
            try {
                json = (JSONObject) jsonArr.get(i);
                System.out.println(json);
                subLayout = (LinearLayout) inflater.inflate(R.layout.pica_inflater, mainLayout, false);
                children.add(subLayout);

                imePica = subLayout.findViewById(R.id.imePica);
                detaljiPica = subLayout.findViewById(R.id.detaljiPica);
                cenaPica = subLayout.findViewById(R.id.cenaPica);
                kolicinaPica = subLayout.findViewById(R.id.kolicinaPica);

                imePica.setText(json.get("ime_pica").toString());
                if (!json.get("opis_pica").equals(null)){
                    detaljiPica.setText(json.get("opis_pica").toString());
                }

                cenaPica.setText(json.get("cena").toString());
                kolicinaPica.setText(json.get("kolicina").toString());

                final Button obrisi = subLayout.findViewById(R.id.dugmeObrisi);
                final String id_brisanje = json.get("id_pica").toString();
                obrisi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppCompatActivity picaActivity = ((AppCompatActivity) MainActivity.mainActiityContext);
                        JSONObject json = new JSONObject();
                        try {
                            json.put("id_pica", id_brisanje);
                            ApiHandler.postRequest("obrisiPice", json, (HttpHandler) PicaControler.instance);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                LinearLayout izmena = subLayout.findViewById(R.id.izmenaPicaLayout);
                final JSONObject finalJson = json;
                izmena.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        izmenaPica(finalJson);
                    }
                });
                if(i%2 == 0){
                    subLayout.setBackgroundColor(Color.parseColor("#D6E4F0"));
                } else{
                    subLayout.setBackgroundColor(Color.parseColor("#F6F6F6"));
                }

                mainLayout.addView(subLayout);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Button dodaj = picaActivity.findViewById(R.id.dodaj);
        dodaj.setText("Dodaj novo pice");



    }


    private void generateDataKorisnici(JSONArray jsonArr){
        AppCompatActivity picaActivity = ((AppCompatActivity) PrikazPica.picaActivityContent);

        LinearLayout dodajL = picaActivity.findViewById(R.id.dugmeDodajLayout);
        LinearLayout prikaz = picaActivity.findViewById(R.id.prikazi);
        dodajL.setVisibility(View.VISIBLE);
        prikaz.setVisibility(View.VISIBLE);

        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(picaActivity);
        mainLayout = picaActivity.findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();
        for (int i = 0; i < jsonArr.length(); i++) {
            JSONObject json = null;
            try {
                json = (JSONObject) jsonArr.get(i);
                System.out.println(json);
                subLayout = (LinearLayout) inflater.inflate(R.layout.korisnici_inflater, mainLayout, false);
                children.add(subLayout);

                imeKorisnika = subLayout.findViewById(R.id.imeKorisnika);
                prezimeKorisnika = subLayout.findViewById(R.id.prezimeKorisnika);
                korisnickoIme = subLayout.findViewById(R.id.korisnickoIme);
                lozinka = subLayout.findViewById(R.id.lozinkaKorisnika);
                ulogaKorisnika = subLayout.findViewById(R.id.ulogaKorisnika);

                imeKorisnika.setText(json.get("ime").toString());
                prezimeKorisnika.setText(json.get("prezime").toString());
                korisnickoIme.setText(json.get("korisnicko_ime").toString());
                lozinka.setText(json.get("lozinka").toString());
                ulogaKorisnika.setText(json.get("opis").toString());


                final Button obrisi = subLayout.findViewById(R.id.obrisiKorisnika);
                final String id_brisanje = json.get("id").toString();
                final String korisnickoIme = json.get("korisnicko_ime").toString();
                obrisi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppCompatActivity picaActivity = ((AppCompatActivity) MainActivity.mainActiityContext);
                        JSONObject json = new JSONObject();
                        try {
                            json.put("id", id_brisanje);
                            json.put("ime", korisnickoIme);
                            ApiHandler.postRequest("obrisiKorisnika", json, (HttpHandler) PicaControler.instance);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                LinearLayout izmena = subLayout.findViewById(R.id.izmenaKorisnikaLayout);
                final JSONObject finalJson = json;
                izmena.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        izmenaKorisnika(finalJson);
                    }
                });

                if(i%2 == 0){
                    subLayout.setBackgroundColor(Color.parseColor("#D6E4F0"));
                } else{
                    subLayout.setBackgroundColor(Color.parseColor("#F6F6F6"));
                }

                mainLayout.addView(subLayout);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Button dodaj = picaActivity.findViewById(R.id.dodaj);
        dodaj.setText("Dodaj novog korisnika");

    }

    private void obrisiKorisnika(String korisnikId){
        AppCompatActivity picaActivity = ((AppCompatActivity) MainActivity.mainActiityContext);
        JSONObject json = new JSONObject();
        try {
            json.put("id", korisnikId);
            ApiHandler.postRequest("obrisiKorisnika", json, (HttpHandler) PicaControler.instance);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void logout(){
        AppCompatActivity picaActivity = ((AppCompatActivity) PrikazPica.picaActivityContent);
        String korisnickoIme = MainActivity.ulogovaniKorisnik;
        System.out.println(korisnickoIme);

        JSONObject json = new JSONObject();
        try {
            json.put("korisnicko_ime", korisnickoIme);
            ApiHandler.postRequest("logout", json, (HttpHandler) PicaControler.instance);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void dodaj(){
        AppCompatActivity picaActivity = ((AppCompatActivity) PrikazPica.picaActivityContent);
        Button dodavanje = picaActivity.findViewById(R.id.dodaj);
        String odluka = dodavanje.getText().toString();
        if (odluka.equals("Dodaj novog korisnika")){
            dodajNovogKorisnika();
        } else {
            dodajNovoPice();
        }
    }

    public void dodajNovogKorisnika(){
        final AppCompatActivity picaActivity = ((AppCompatActivity) PrikazPica.picaActivityContent);

        LinearLayout dodaj = picaActivity.findViewById(R.id.dugmeDodajLayout);
        LinearLayout prikaz = picaActivity.findViewById(R.id.prikazi);
        dodaj.setVisibility(View.INVISIBLE);
        prikaz.setVisibility(View.INVISIBLE);



        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(picaActivity);
        mainLayout = picaActivity.findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();

        subLayout = (LinearLayout) inflater.inflate(R.layout.dodavanja_korisnika, mainLayout, false);

        opcija = subLayout.findViewById(R.id.opcija);
        opcija.setText("Podaci o korisniku");

        Button dodavanje = subLayout.findViewById(R.id.dodavanje);
        dodavanje.setText("Dodaj novog korisnika");
        dodavanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ime = ((TextView)subLayout.findViewById(R.id.dodavanjeIme)).getText().toString();
                String prezime = ((TextView)subLayout.findViewById(R.id.dodavanjePrezime)).getText().toString();
                String korisnickoIme = ((TextView)subLayout.findViewById(R.id.dodavanjeKorisnickoIme)).getText().toString();
                String lozinka = ((TextView)subLayout.findViewById(R.id.dodavanjeLozinka)).getText().toString();
                RadioGroup rg = subLayout.findViewById(R.id.dodavanjeUloga);
                RadioButton selected =  rg.findViewById(rg.getCheckedRadioButtonId());
                if(selected == null || ime.equals("") || prezime.equals("") || korisnickoIme.equals("") || lozinka.equals("")){
                    Toast.makeText(PrikazPica.picaActivityContent, "Morate uneti sve podatke!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String pravo = selected.getText().toString();
                String uloga;
                if (pravo.equals("administrator")){
                    uloga = "1";
                } else{
                    uloga= "2";
                }

                JSONObject json = new JSONObject();
                try {
                    json.put("ime", ime);
                    json.put("prezime", prezime);
                    json.put("korisnicko_ime", korisnickoIme);
                    json.put("lozinka", lozinka);
                    json.put("uloga", uloga);
                    ApiHandler.postRequest("dodavanjeKorisnika", json, (HttpHandler) PicaControler.instance);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        children.add(subLayout);
        mainLayout.addView(subLayout);

    }
    public void dodajNovoPice(){
        final AppCompatActivity picaActivity = ((AppCompatActivity) PrikazPica.picaActivityContent);

        LinearLayout dodaj = picaActivity.findViewById(R.id.dugmeDodajLayout);
        LinearLayout prikaz = picaActivity.findViewById(R.id.prikazi);
        dodaj.setVisibility(View.INVISIBLE);
        prikaz.setVisibility(View.INVISIBLE);



        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(picaActivity);
        mainLayout = picaActivity.findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();

        subLayout = (LinearLayout) inflater.inflate(R.layout.dodavanje_pica, mainLayout, false);

        opcija = subLayout.findViewById(R.id.opcija);
        opcija.setText("Podaci o picu");
        Button dodavanjePica = subLayout.findViewById(R.id.dodajPice);
        dodavanjePica.setText("Dodaj novo pice");
        dodavanjePica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imePica = ((TextView)subLayout.findViewById(R.id.piceIme)).getText().toString();
                String opisPica = ((TextView)subLayout.findViewById(R.id.piceOpis)).getText().toString();
                if (opisPica.equals(null)){
                    opisPica = null;
                }
                String kolicinaPica = ((TextView)subLayout.findViewById(R.id.piceKolicina)).getText().toString();
                String cenaPica = ((TextView)subLayout.findViewById(R.id.piceCena)).getText().toString();
                RadioGroup rg = subLayout.findViewById(R.id.tipPica);
                RadioButton selected =  rg.findViewById(rg.getCheckedRadioButtonId());


                if(selected == null || imePica.equals("") || kolicinaPica.equals("") || cenaPica.equals("")){
                    Toast.makeText(PrikazPica.picaActivityContent, "Morate popuniti sve podatke!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String tip = selected.getText().toString();
                String piceTip = tipPica(tip);


                JSONObject json = new JSONObject();
                try {
                    json.put("ime_pica", imePica);
                    json.put("opis_pica", opisPica);
                    json.put("kolicina", kolicinaPica);
                    json.put("cena", cenaPica);
                    json.put("tip_pica", piceTip);
                    ApiHandler.postRequest("dodavanjePica", json, (HttpHandler) PicaControler.instance);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        children.add(subLayout);
        mainLayout.addView(subLayout);

    }

    private String tipPica(String tip){
        switch(tip){
            case "Topli napici":
                return "1";
            case "Sokovi":
                return "2";
            case "Voda":
                return "3";
            case "Pivo":
                return "4";
            case "Alkoholna pica":
                return "5";
            case "Kokteli":
                return "6";

        }
        return "";

    }

    private void izmenaKorisnika(JSONObject json){

        final AppCompatActivity picaActivity = ((AppCompatActivity) PrikazPica.picaActivityContent);
        JSONObject korisnik = json;
        LinearLayout dodaj = picaActivity.findViewById(R.id.dugmeDodajLayout);
        LinearLayout prikaz = picaActivity.findViewById(R.id.prikazi);
        dodaj.setVisibility(View.INVISIBLE);
        prikaz.setVisibility(View.INVISIBLE);



        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(picaActivity);
        mainLayout = picaActivity.findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();

        subLayout = (LinearLayout) inflater.inflate(R.layout.dodavanja_korisnika, mainLayout, false);

        opcija = subLayout.findViewById(R.id.opcija);
        opcija.setText("Podaci o korisniku");
        imeKorisnika = subLayout.findViewById(R.id.dodavanjeIme);
        prezimeKorisnika = subLayout.findViewById(R.id.dodavanjePrezime);
        korisnickoIme = subLayout.findViewById(R.id.dodavanjeKorisnickoIme);
        korisnickoIme = subLayout.findViewById(R.id.dodavanjeKorisnickoIme);
        korisnickoIme.setEnabled(false);
        lozinka = subLayout.findViewById(R.id.dodavanjeLozinka);
        try {
            imeKorisnika.setText((CharSequence) korisnik.get("ime"));
            prezimeKorisnika.setText((CharSequence) korisnik.get("prezime"));
            korisnickoIme.setText((CharSequence) korisnik.get("korisnicko_ime"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Button izmeni = subLayout.findViewById(R.id.dodavanje);
        izmeni.setText("Izmeni korisnika");
        izmeni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ime = ((TextView)subLayout.findViewById(R.id.dodavanjeIme)).getText().toString();
                String prezime = ((TextView)subLayout.findViewById(R.id.dodavanjePrezime)).getText().toString();
                String korisnickoIme = ((TextView)subLayout.findViewById(R.id.dodavanjeKorisnickoIme)).getText().toString();
                String lozinka = ((TextView)subLayout.findViewById(R.id.dodavanjeLozinka)).getText().toString();
                RadioGroup rg = subLayout.findViewById(R.id.dodavanjeUloga);
                RadioButton selected =  rg.findViewById(rg.getCheckedRadioButtonId());
                if(selected == null || ime.equals("") || prezime.equals("") || korisnickoIme.equals("") || lozinka.equals("")){
                    Toast.makeText(PrikazPica.picaActivityContent, "Morate uneti sve podatke!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String pravo = selected.getText().toString();
                String uloga;
                if (pravo.equals("administrator")){
                    uloga = "1";
                } else{
                    uloga= "2";
                }

                JSONObject json = new JSONObject();
                try {
                    json.put("ime", ime);
                    json.put("prezime", prezime);
                    json.put("korisnicko_ime", korisnickoIme);
                    json.put("lozinka", lozinka);
                    json.put("uloga", uloga);
                    ApiHandler.postRequest("izmenaKorisnika", json, (HttpHandler) PicaControler.instance);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        children.add(subLayout);
        mainLayout.addView(subLayout);

    }

    private void izmenaPica(JSONObject json){
        final AppCompatActivity picaActivity = ((AppCompatActivity) PrikazPica.picaActivityContent);
        JSONObject pice = json;
        LinearLayout dodaj = picaActivity.findViewById(R.id.dugmeDodajLayout);
        LinearLayout prikaz = picaActivity.findViewById(R.id.prikazi);
        dodaj.setVisibility(View.INVISIBLE);
        prikaz.setVisibility(View.INVISIBLE);



        children = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(picaActivity);
        mainLayout = picaActivity.findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();

        subLayout = (LinearLayout) inflater.inflate(R.layout.dodavanje_pica, mainLayout, false);

        opcija = subLayout.findViewById(R.id.opcija);
        opcija.setText("Podaci o picu");
        Button izmeniPice = subLayout.findViewById(R.id.dodajPice);
        izmeniPice.setText("Izmeni pice");

        imePica = subLayout.findViewById(R.id.piceIme);
        imePica.setEnabled(false);
        opisPicaEdit = subLayout.findViewById(R.id.piceOpis);
        kolicinaPicaEdit = subLayout.findViewById(R.id.piceKolicina);
        cenaPicaEdit = subLayout.findViewById(R.id.piceCena);

        try {
            imePica.setText((CharSequence) pice.get("ime_pica"));
            if (!pice.get("opis_pica").equals(null)){
                opisPicaEdit.setText((CharSequence) pice.get("opis_pica"));
            }

            kolicinaPicaEdit.setText(pice.get("kolicina").toString());
            cenaPicaEdit.setText(json.get("cena").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        izmeniPice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String imePica = ((TextView)subLayout.findViewById(R.id.piceIme)).getText().toString();
                String opisPica = ((TextView)subLayout.findViewById(R.id.piceOpis)).getText().toString();
                if (opisPica.equals(null)){
                    opisPica = null;
                }
                String kolicina = ((TextView)subLayout.findViewById(R.id.piceKolicina)).getText().toString();
                String cena = ((TextView)subLayout.findViewById(R.id.piceCena)).getText().toString();
                RadioGroup rg = subLayout.findViewById(R.id.tipPica);
                RadioButton selected =  rg.findViewById(rg.getCheckedRadioButtonId());
                if (selected == null || kolicina.equals("") || cena.equals("")){
                    Toast.makeText(PrikazPica.picaActivityContent, "Morate uneti sve podatke!", Toast.LENGTH_SHORT).show();
                    return;
                }
                String tip = selected.getText().toString();
                String piceTip = tipPica(tip);

                JSONObject json = new JSONObject();
                try {
                    json.put("ime_pica", imePica);
                    json.put("opis_pica", opisPica);
                    json.put("kolicina", kolicina);
                    json.put("cena", cena);
                    json.put("tip_pica", piceTip);
                    ApiHandler.postRequest("izmenaPica", json, (HttpHandler) PicaControler.instance);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });




        children.add(subLayout);
        mainLayout.addView(subLayout);
    }
}
