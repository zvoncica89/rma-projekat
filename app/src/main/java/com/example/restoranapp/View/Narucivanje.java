package com.example.restoranapp.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.restoranapp.Controler.NarucivanjeControler;
import com.example.restoranapp.MainActivity;
import com.example.restoranapp.R;

import org.json.JSONArray;

public class Narucivanje extends AppCompatActivity {
    public static Context narucivanjeActivityContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_narucivanje);
        Narucivanje.narucivanjeActivityContent = this;
        final NarucivanjeControler narucivanjeControler = NarucivanjeControler.getInstance();
        narucivanjeControler.naruciPica();
        narucivanjeControler.dobaviPica();

        Button naruceno = findViewById(R.id.narucenoDugme);
        naruceno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                narucivanjeControler.prikazPorudzbine();
            }
        });

        Button naruci = findViewById(R.id.zavrsiPorudzbinu);
        naruci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                narucivanjeControler.zavrsiPorudzbinu();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        final NarucivanjeControler narucivanjeControler = NarucivanjeControler.getInstance();
        Integer duzina = narucivanjeControler.duzinaPorudzbine();
        if(duzina>0){
            Intent intent = new Intent(this, Narucivanje.class);
            this.startActivity(intent);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            this.startActivity(intent);
            finish();
        }


    }


}
