package com.example.restoranapp.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.restoranapp.Controler.PicaControler;
import com.example.restoranapp.R;

public class PrikazPica extends AppCompatActivity {
    public static Context picaActivityContent;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, PrikazPica.class);
        this.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prikaz_pica);
        PrikazPica.picaActivityContent = this;

        final PicaControler picaControler = PicaControler.getInstance();
        Button pica = findViewById(R.id.prikazPica);
        pica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picaControler.dobaviPica();
            }
        });

        Button korisnici = findViewById(R.id.prikazKorisnika);
        korisnici.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picaControler.dobaviKorisnike();
            }
        });

        Button logout = findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picaControler.logout();
            }
        });

        Button dodaj = findViewById(R.id.dodaj);
//        String odluka = dodaj.getText().toString();
//        if (odluka.equals("Dodaj novog korisnika")){
//            dodaj.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    picaControler.dodajNovogKorisnika();
//                }
//            });
//        } else {
//            dodaj.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    picaControler.dodajNovoPice();
//                }
//            });
//        }
        dodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picaControler.dodaj();
            }
        });



    }
}
